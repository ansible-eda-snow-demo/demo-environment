# demo-environment

Ansible Even Driven Automation Demo

## Demo/Lab Developers:

*Rafael Minguillon*, EMEA Ansible Tecnical Account Manager, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- ServiceNow instance.
- Twilio SendGrid account.
- Amazon Web Service account.
- OpenShift Cluster +4.12 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### ServiceNow

You can request a ServiceNow Developer Instance in the following [link](https://developer.servicenow.com/).

### Twilio SendGrid account

You can request a Twilio SendGrid account in the following [link](https://sendgrid.com/).

### Amazon Web Service account

If you don't have an Amazon Web Service account, you can request one in the following [link](https://demo.redhat.com/catalog?category=Open_Environments&item=babylon-catalog-prod%2Fsandboxes-gpte.sandbox-open.prod).

### OpenShift

If you don't have an OpenShift Cluster 4.12 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp412-wksp.prod).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

- After installing it, create the `inventory` and the `ansible-navigator` definition file:

```sh
cd ~
mkdir ansible-navigator
cat << EOF > ansible-navigator/inventory
[controller]
localhost ansible_connection=local
EOF
cat << EOF > ansible-navigator/ansible-navigator.yml
---
ansible-navigator:
  ansible:
    inventory:
      entries:
      - ./inventory
  app: run
  editor:
    command: vim_from_setting
    console: false
  execution-environment:
    container-engine: podman
    image: registry.redhat.io/ansible-automation-platform-23/ee-supported-rhel8:latest
    pull:
      policy: missing
  logging:
    append: true
    file: /tmp/navigator/ansible-navigator.log
    level: debug
  playbook-artifact:
    enable: false
EOF
```

## Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/ansible-eda-snow-demo/demo-environment.git
```

## Deploy Demo Environment

- Create the demo configuration variables file, replacing the highlighted variables:

```sh
cat << EOF > demo-environment/vars/demo-config.yml
---
# Demo vars
demo_namespace: ansible-eda-demo
ansible_eda_demo_templates_path: templates

# Snow vars
snow_instance_host: "*<SNOW_HOST>*"
snow_instance_username: "*<SNOW_USERNAME>*"
snow_instance_password: "*<SNOW_PASSWORD>*"
snow_eda_username: "ansible-eda"
snow_eda_password: "RedHat!123"

# AWS vars
aws_access_key: "*<AWS_ACCESS_KEY>*"
aws_secret_key: "*<AWS_SECRET_KEY>*"
aws_security_token: ""
aws_bucket_region: "eu-west-1"

# Sengrid vars
sendgrid_api_key: "*<SENDGRID_API_KEY>*"

# OCP vars
ocp_templates_path: "{{ ansible_eda_demo_templates_path }}/images"

# Gitea vars
gitea_templates_path: "{{ ansible_eda_demo_templates_path }}/gitea"

# Dev Spaces vars
devspaces_operator_templates_path: "{{ ansible_eda_demo_templates_path }}/devspaces/operator"
devspaces_instance_templates_path: "{{ ansible_eda_demo_templates_path }}/devspaces/instance"

# Prometheus vars
prometheus_operator_templates_path: "{{ ansible_eda_demo_templates_path }}/prometheus/operator"
prometheus_instance_templates_path: "{{ ansible_eda_demo_templates_path }}/prometheus/instance"

# Ansible vars
ansible_operator_templates_path: "{{ demo_templates_path }}/ansible/operator"
ansible_eda_instance_templates_path: "{{ demo_templates_path }}/ansible/instance"
ansible_eda_instance_admin_password: redhat
ansible_instance_templates_path: "{{ demo_templates_path }}/ansible/instance"
ansible_instance_admin_password: redhat
ansible_instance_manifest: "*<AAP_MANIFEST>*" --> !!!ENCODE IN BASE64!!! (base64 /path/to/ansible_manifest.zip)
EOF
```

- Deploy the demo environment:

```sh
cd ~/demo-environment/ansible-navigator
ansible-navigator run ../ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshift_storage_class=gp3-csi'
```
